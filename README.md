# Administracion de Recursos 2020

Para ver los archivos markdown en formato GithHub utilizar `grip`

```bash
pip install grip
grip {filename}
```

Podes compilarlos a PDFs si gustas o podes simpelmente leerlos desde el Repo.

#### Sentite libre de aportar!!

- SQA por cada unidad.

## Fechas de parciales
- Primer Evaluacion: 
	- Taller: 08/04
		- Armar esquema perliminar de la solucion
	- Exposicion: 15/04
- Segunda Evaluacion:
	- Unidades tematicas II y III Individual.
- Tercera Evaluacion:
	- Unidad tematica IV: Individual - 26/08
- Cuarta Evaluacion:
	- Unidades tematicas V y VI - 04/11 Individual
- TP FINAL: 11/11
- Primer recuperatorio 18/11
- Segundo recuperatorio 25/11
- Cierre de ciclo: 02/12

## Condiciones de aprobacion
- 75% de asistencias
- 100% de autoealuacione, coevaluaciones, heteroevaluaciones
- 100% de grado autonomo estrategico
- Aprobar taller grupal
- Aprobar instancias individuales
- Aprobar trabajo final
- Aprobar el examen integrador
- Desaprobar solo una instancia

## Condiciones de regularizacion
- 75% de asistencias
- 75% de autoealuacione, coevaluaciones, heteroevaluaciones
- 100% de grado autonomo estrategico
- Aprobar taller grupal
- Aprobar 2/3 instancias individuales
- Aprobar trabajo final
